# Descrição

O repositório consiste na implementação do trabalho final de Fundamentos de Sistemas Embarcados de 2020. O enunciado do trabalho pode ser encontrado no [link](https://gitlab.com/fse_fga/projetos/trabalho-final)

# Como rodar

## Raspberry Pi

Para rodar o codigo da Raspberry PI é necessário copiar a pasta rasp e dentro dela executar o comando 

```bash
make
```

### Dentro da interface

A interface é controlada pelas setas do teclado e o botão enter. Além disso pode ser necessário ultilizar o teclado para entrar com o nome dos comodos. 
Ao conectar uma nova esp o mac address aparece no menu a esquerda, e é atualizada no menu a direita ao apertar a tecla "cima" no teclado.
O comodo da esp pode ser setado uma vez ao conectar e selecionar a opção "configurar <mac_address>"

## ESP 32

Para rodar o código da esp basta seguir as instruções de build e menu config para esp32. Para mais informações acesse o [link]()

Junto a esp deve ser usado um sensor de temperatura DHT11 ligado à GPIO 4

### Detalhes da esp

Ao rodar o codigo junto a rasp é preciso esperar um tempo para a esp conectar ao wifi e se comunicar com a rasp via protocolo mqtt. Logo após o led é controlado pela rasp e o estado do botão é mostrado na rasp também. 
