#include <iostream>
#include "interface.hpp"
#include <iomanip>
#include <unistd.h>
#include <curses.h>
#include <fstream>
#include <string>
#include <unordered_set>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include "mqtt.hpp"
#include <tuple>
#include "log.hpp"

using namespace std;

Info::Info(bool *alarm_state, unordered_map<int, GPIOPin *> gpio_pins,
           vector<string> *mac_adresses, unordered_map<string, string> *device_place,
           unordered_map<string, esp32_t> *esp_control)
{
    this->esp_control = esp_control;
    this->device_place = device_place;
    this->mac_adresses = mac_adresses;
    this->alarm_state = alarm_state;
    this->gpio_pins = gpio_pins;
    this->width = INFO_WIDTH;
    this->heigth = INFO_LINES + 7;
    this->window = newwin(this->heigth, this->width, INFO_X_START, INFO_Y_START);
    box(this->window, 0, 0);
    this->update();
}

int Info::getWidth()
{
    return this->width;
}

int Info::getHeight()
{
    return this->heigth;
}

void Info::update()
{
    wclear(this->window);
    box(this->window, 0, 0);
    mvwprintw(this->window, 1, 2, "Informacoes do sistema");
    vector<stringstream> streams(INFO_LINES);

    /* Updating info screen */
    int i = 0;
    streams[i++] << "(ID) - Sala:";
    streams[i++] << " 18  - Lampada: " << (this->gpio_pins[18]->getPinState() == 1 ? "LIGADA" : "DESLIGADA");
    streams[i++] << " 25  - Sensor de Presenca: " << (this->gpio_pins[25]->getPinState() == 1 ? "ATIVO" : "DESATIVADO");
    streams[i++] << " 12  - Sensor Abertura (Porta): " << (this->gpio_pins[12]->getPinState() == 1 ? "ATIVO" : "DESATIVADO");
    streams[i++] << " 16  - Sensor Abertura (Janela): " << (this->gpio_pins[16]->getPinState() == 1 ? "ATIVO" : "DESATIVADO");
    streams[i++] << "";
    streams[i++] << "(ID) - Cozinha: ";
    streams[i++] << " 17  - Lampada: " << (this->gpio_pins[17]->getPinState() == 1 ? "LIGADA" : "DESLIGADA");
    streams[i++] << " 26  - Sensor de Presenca: " << (this->gpio_pins[26]->getPinState() == 1 ? "ATIVO" : "DESATIVADO");
    streams[i++] << " 05  - Sensor Abertura (Porta): " << (this->gpio_pins[05]->getPinState() == 1 ? "ATIVO" : "DESATIVADO");
    streams[i++] << " 06  - Sensor Abertura (Cozinha): " << (this->gpio_pins[06]->getPinState() == 1 ? "ATIVO" : "DESATIVADO");
    streams[i++] << "";
    streams[i++] << "Estado do alarme: " << (*this->alarm_state ? "ATIVO" : "DESATIVADO");
    streams[i++] << "";
    streams[i++] << " ***   Pressione a tecla para cima para o  ***";
    streams[i++] << " *** novo dispositivo aparecer na esquerda ***";
    streams[i++] << "";
    streams[i++] << "(ID) - Dispositivo - Comodo";
    streams[i++] << "";
    for (int k = 0; k < 5; ++k, i += 2)
    {
        if (k < (int)this->mac_adresses->size())
        {
            string &mac = (*this->mac_adresses)[k];
            auto &mp = (*this->device_place);

            streams[i] << "0" << k + 1 << " - " << (*this->mac_adresses)[k];
            if (mp.find(mac) != mp.end())
            {
                streams[i] << " - " << this->device_place->at(mac);
            }
            if (this->esp_control->find(mac) != this->esp_control->end())
            {
                esp32_t esp = this->esp_control->at(mac);
                streams[i + 1] << "Temp: " << esp.temperatura << "ºC Umd: " << esp.umidade << "% Ent: " << esp.entrada << " Saida: " << esp.saida;
            }
        }
    }
    for (int j = 0; j < INFO_LINES; ++j)
        mvwprintw(this->window, 3 + j, 2, "%s", streams[j].str().c_str());

    wrefresh(this->window);
}
Input::Input()
{
    this->window = newwin(INPUT_HEIGHT, INPUT_WIDTH, INPUT_X_START, INPUT_Y_START);
    this->isActive = false;
    this->updateScreen();
}
Input::Input(int x_start, int y_start)
{
    this->window = newwin(INPUT_HEIGHT, INPUT_WIDTH, x_start, y_start);
    this->isActive = false;
    this->updateScreen();
}

void Input::updateScreen()
{
    wclear(this->window);
    if (this->isActive)
    {
        box(this->window, 0, 0);
        mvwprintw(this->window, 1, 2, "> ");
    }
    wrefresh(this->window);
}

void Input::activate()
{
    this->isActive = true;
    this->updateScreen();
}

void Input::deactivate()
{
    this->isActive = false;
    this->updateScreen();
}

string Input::getString()
{
    if (!this->isActive)
        return "";
    char buffer[500];
    wscanw(this->window, "%[^\n]\n", buffer);
    string res;
    for (size_t i = 0; i < strlen(buffer); ++i)
        res.push_back(buffer[i]);
    this->updateScreen();
    return res;
}
double Input::getDouble()
{
    if (!this->isActive)
        return -1;
    double value;
    wscanw(this->window, "%lf", &value);
    this->updateScreen();
    return value;
}
int Input::getInt()
{
    if (!this->isActive)
        return -1;
    int value;
    wscanw(this->window, "%d", &value);
    this->updateScreen();
    return value;
}

Menu::Menu(bool *alarm_state, unordered_map<int, GPIOPin *> gpio_pins, vector<string> *mac_adresses, ClientMQTT *client,
           unordered_map<string, string> *device_place, unordered_map<string, esp32_t> *esp_control)
{
    this->log_name = initLog();
    this->esp_control = esp_control;
    this->device_place = device_place;
    this->client = client;
    this->mac_adresses = mac_adresses;
    this->configured = unordered_map<int, bool>();
    this->alarm_state = alarm_state;
    this->gpio_pins = gpio_pins;
    this->input_screen = Input(MENU_LINES + 7, INPUT_Y_START);
    this->input_screen.deactivate();
    this->current_screen = MAIN_MENU;
    this->alarm_state = alarm_state;
    this->current_option = 0;
    this->height = MENU_LINES + 7;
    this->width = MENU_WIDTH;
    this->window = newwin(this->height, this->width, MENU_X_START, MENU_Y_START);
    box(this->window, 0, 0);
    keypad(this->window, TRUE);
    this->updateScreen();
}

string Menu::getLogName(){
    return this->log_name;
}
int Menu::getHeiht()
{
    return this->height;
}

int Menu::getWidth()
{
    return this->width;
}

void Menu::updateScreen()
{
    wclear(this->window);
    box(this->window, 0, 0);
    if (this->current_screen == MAIN_MENU)
    {
        mvwprintw(this->window, 1, 20, "Menu de acoes");
        int i = 0;
        if (this->current_option == 0)
        {
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4 + i, 2, "* Ligar dispositivo");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else
        {
            mvwprintw(this->window, 4 + i, 2, "  Ligar dispositivo");
        }
        ++i;
        if (this->current_option == 1)
        {
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4 + i, 2, "* Desligar dispositivo");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else
        {
            mvwprintw(this->window, 4 + i, 2, "  Desligar dispositivo");
        }
        ++i;
        if (this->current_option == 2)
        {
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4 + i, 2, "* Ligar alarme");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else
        {
            mvwprintw(this->window, 4 + i, 2, "  Ligar alarme");
        }
        ++i;
        if (this->current_option == 3)
        {
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4 + i, 2, "* Desligar alarme");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else
        {
            mvwprintw(this->window, 4 + i, 2, "  Desligar alarme");
        }
        for (int k = 0; k < 5; ++k)
        {
            ++i;
            if (this->current_option == 4 + k)
            {
                string showing_text = "* ";
                if (k < (int)this->mac_adresses->size())
                {
                    if (!this->configured[k])
                        showing_text += "Configurar esp - ";
                    showing_text += (*this->mac_adresses)[k];
                }
                else
                {
                    showing_text += "Espaco para novo dispositivo";
                }
                wattron(window, COLOR_PAIR(COLOR_SELECTED));
                mvwprintw(this->window, 4 + i, 2, showing_text.c_str());
                wattroff(window, COLOR_PAIR(COLOR_SELECTED));
            }
            else
            {
                string showing_text = "  ";
                if (k < (int)this->mac_adresses->size())
                {
                    if (!this->configured[k])
                        showing_text += "Configurar esp - ";
                    showing_text += (*this->mac_adresses)[k];
                }
                else
                {
                    showing_text += "Espaco para novo dispositivo";
                }
                mvwprintw(this->window, 4 + i, 2, showing_text.c_str());
            }
        }
        ++i;
        if (this->current_option == 9)
        {
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4 + i, 2, "* Sair");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else
        {
            mvwprintw(this->window, 4 + i, 2, "  Sair");
        }
        ++i;
    }
    else if (this->current_screen == TURN_ON_DEVICE)
    {
        mvwprintw(this->window, this->height / 2, this->width / 2 - 21, "Insira o ID do dispositivo a ser ligado");
    }
    else if (this->current_screen == TURN_OFF_DEVICE)
    {
        mvwprintw(this->window, this->height / 2, this->width / 2 - 21, "Insira o ID do dispositivo a ser desligado");
    }
    else if (this->current_screen == SUCESS)
    {
        mvwprintw(this->window, this->height / 2, this->width / 2 - 18, "Sua acao foi processada com sucesso");
    }
    else if (this->current_screen == BAD_INPUT)
    {
        mvwprintw(this->window, this->height / 2, this->width / 2 - 7, "Valor invalido");
    }
    else if (this->current_screen == ESP_CONFIG)
    {
        string lines[7];
        int i = 0;
        lines[i++] = "Entre com o uma string com nome do comodo onde a placa ";
        lines[i++] = "esta, 2 placas nao podem ter nome de comodos iguais";
        lines[i++] = "Ex sala";
        for (int i = 0; i < 3; ++i)
            mvwprintw(this->window, i - 1 + this->height / 2, this->width / 2 - (int)lines[i].size() / 2, lines[i].c_str());
    }
    wrefresh(this->window);
}

void Menu::update()
{
    if (this->current_screen == MAIN_MENU)
    {
        int key = wgetch(this->window);
        if (key == KEY_DOWN)
        {
            this->current_option += 1;
            this->current_option %= MENU_LINES;
        }
        if (key == KEY_UP)
        {
            this->current_option += MENU_LINES - 1;
            this->current_option %= MENU_LINES;
        }
        if (key == KEY_RIGHT or key == '\n' or key == KEY_ENTER)
        {
            if (this->current_option == 0)
            {
                this->current_screen = TURN_ON_DEVICE;
            }
            else if (this->current_option == 1)
            {
                this->current_screen = TURN_OFF_DEVICE;
            }
            else if (this->current_option == 2)
            {
                *this->alarm_state = true;
            }
            else if (this->current_option == 3)
            {
                *this->alarm_state = false;
            }
            else if (this->current_option >= 4 and this->current_option < 9)
            {
                int k = this->current_option - 4;
                if (k < (int)this->mac_adresses->size() and !this->configured[k])
                {
                    this->current_screen = ESP_CONFIG;
                }
            }
            else if (this->current_option == 9)
            {
                kill(getpid(), SIGINT);
            }
        }
    }
    else if (current_screen == TURN_ON_DEVICE)
    {
        this->input_screen.activate();
        int device_id = this->input_screen.getInt();

        if (device_id > 0 and device_id <= (int)this->mac_adresses->size())
        {
            string mac_address = this->mac_adresses->at(device_id - 1);
            string topic_name = "fse2020/170035158/dispositivos/" + mac_address;
            (*this->esp_control)[mac_address].saida = 1;
            string message = "{\"saida\": 1}";
            this->client->publish(topic_name, message);
            this->current_screen = SUCESS;
            /* Log register */
            registerLog(this->log_name, "Ligar", mac_address);
        }
        else if (device_id == 18 or device_id == 17)
        {
            
            this->current_screen = SUCESS;
            this->gpio_pins[device_id]->setPinState(HIGH);
            /* Log register */
            string device = device_id == 18 ? "Lampada da sala" : "Lampada da cozinha";
            registerLog(this->log_name, "Ligar", device);
        }
        else
        {
            this->current_screen = BAD_INPUT;
        }
    }
    else if (current_screen == TURN_OFF_DEVICE)
    {
        this->input_screen.activate();
        int device_id = this->input_screen.getInt();
        if (device_id > 0 and device_id <= (int)this->mac_adresses->size())
        {
            string mac_address = this->mac_adresses->at(device_id - 1);
            string topic_name = "fse2020/170035158/dispositivos/" + mac_address;
             (*this->esp_control)[mac_address].saida = 0;
            string message = "{\"saida\": 0}";
            this->client->publish(topic_name, message);
            this->current_screen = SUCESS;
            /* Log register */
            registerLog(this->log_name, "Desligar", mac_address);
        }
        else if (device_id == 18 or device_id == 17)
        {
            this->current_screen = SUCESS;
            this->gpio_pins[device_id]->setPinState(LOW);
            /* Log register */
            string device = device_id == 18 ? "Lampada da sala" : "Lampada da cozinha";
            registerLog(this->log_name, "Desligar", device);
        }
        else
        {
            this->current_screen = BAD_INPUT;
        }
    }
    else if (current_screen == ESP_CONFIG)
    {
        ofstream debug("debug_file");

        this->input_screen.activate();
        string place = this->input_screen.getString();
        int k = this->current_option - 4;
        string mac_address = (*mac_adresses)[k];

        /* Making message and topic */
        string topic = "";
        topic += "fse2020/170035158/dispositivos/";
        topic += mac_address;

        string message = "";
        message += "{\"comodo\" : \"";
        message += place;
        message += "\"}";
        
        /* Sending message */
        this->client->publish(topic, message);
        (*this->esp_control)[mac_address] = esp32_t();
        (*this->device_place)[mac_address] = place;
        this->configured[k] = true;
        this->current_screen = SUCESS;
        registerLog(this->log_name, "Configuracao de dispositivo", mac_address);
    }
    else if (current_screen == UPDATE_TEMP_FEEDBACK or this->current_screen == BAD_INPUT or this->current_screen == SUCESS)
    {
        this->input_screen.deactivate();
        sleep(2);
        this->current_screen = MAIN_MENU;
    }
    this->updateScreen();
}

void showEndScreen()
{
    int row, col;
    getmaxyx(stdscr, row, col);
    WINDOW *window = newwin(row, col, 0, 0);
    wclear(window);
    box(window, 0, 0);
    mvwprintw(window, row / 2, col / 2 - 15, "Programa encerrado com sucesso");
    mvwprintw(window, row / 2 + 1, col / 2 - 11, "Aperte enter para sair");
    wrefresh(window);
    getch();
    clear();
    refresh();
    endwin();
}

void InterfaceInit()
{
    initscr();
    if (has_colors())
    {
        start_color();
    }
    else
    {
    }
    curs_set(0);
    echo();
    cbreak();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_BLACK, COLOR_WHITE);

    refresh();
    int row, col;
    getmaxyx(stdscr, row, col);
    WINDOW *window = newwin(row, col, 0, 0);
    wclear(window);
    box(window, 0, 0);
    mvwprintw(window, row / 2, col / 2 - 30, "Para uma melhor experiencia aumente o tamanho do seu terminal");
    mvwprintw(window, row / 2 + 1, col / 2 - 16, "Aperte uma tecla para para continuar");
    refresh();
    wrefresh(window);
    getch();
    wclear(window);
    wrefresh(window);
}
