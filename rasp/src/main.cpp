#include <iostream>
#include "gpio.hpp"
#include "i2c.hpp"
#include <thread>
#include <signal.h>
#include <unordered_map>
#include <math.h>
#include <algorithm>
#include <MQTTClient.h>
#include <vector>
#include "mqtt.hpp"
#include "interface.hpp"
#include <fstream>
#include "json.hpp"
#include "log.hpp"

using namespace std;
using json = nlohmann::json;

/* Ncurses screens */
Menu *menu;
Info *info;

/* Pin variables */
GPIOPin
	*lamp1,
	*lamp2,
	*motion1, *motion2,
	*opening1, *opening2, *opening3, *opening4;

BME280 *bme_sensor;

/* Client MQTT */
ClientMQTT *client;

/* Threads Variables */
thread menu_thread, info_thread, sensor_thread, temperature_thread, alarm_thread;
bool sensor_start, sensor_finish;
bool mt_start, mt_finished;
bool it_start, it_finished;
bool at_start, at_finished;

/* Global variables */
unordered_map<int, GPIOPin *> gpio_pins;
unordered_map<string, string> *device_place;
unordered_map<string, esp32_t> *esp_control;
unordered_map<string, tuple<int, int>> *place_control;
vector<string> *mac_adresses;

bool running = true;
bool alarm_state = false;
bool is_playing = false;
short int cnt = 0;

/* Functions prototypes */
void connectionHandler(int);
void sig_handler(int signum);
void stop_execution(int signum);
void sensor_alert(int device_id);
void default_handler(int socket);
int on_message(void *context, char *topicName, int topic_size, MQTTClient_message *message);
void message_handler(string topic_name, json payload);
bool motion_check();

/* Constants */
const string MQTT_SUBSCRIBE_TOPIC = "fse2020/170035158/#";
const string MQTT_ADDRESS = "mqtt.eclipseprojects.io:1883";
const string CLIENTID = "RASP";

int main()
{
	signal(SIGALRM, sig_handler);
	signal(SIGTERM, stop_execution);
	signal(SIGKILL, stop_execution);
	signal(SIGINT, stop_execution);

	initGPIO();
	/* Init Variables */
	mac_adresses = new vector<string>;
	device_place = new unordered_map<string, string>;
	esp_control = new unordered_map<string, esp32_t>;
	place_control = new unordered_map<string, tuple<int, int>>;

	/* MQTT init*/
	client = new ClientMQTT(MQTT_ADDRESS, CLIENTID, on_message);
	client->connect();
	client->subscribe(MQTT_SUBSCRIBE_TOPIC);

	/* Pins Alocate */
	lamp1 = new GPIOPin(LAMP1, OUTPUT);
	gpio_pins[LAMP1] = lamp1;
	lamp2 = new GPIOPin(LAMP2, OUTPUT);
	gpio_pins[LAMP2] = lamp2;

	motion1 = new GPIOPin(MOTION1, INPUT);
	gpio_pins[MOTION1] = motion1;
	motion2 = new GPIOPin(MOTION2, INPUT);
	gpio_pins[MOTION2] = motion2;

	opening1 = new GPIOPin(OPENING1, INPUT);
	gpio_pins[OPENING1] = opening1;
	opening2 = new GPIOPin(OPENING2, INPUT);
	gpio_pins[OPENING2] = opening2;
	opening3 = new GPIOPin(OPENING3, INPUT);
	gpio_pins[OPENING3] = opening3;
	opening4 = new GPIOPin(OPENING4, INPUT);
	gpio_pins[OPENING4] = opening4;

	/* Interface Init */
	InterfaceInit();
	menu = new Menu(&alarm_state, gpio_pins, mac_adresses, client, device_place, esp_control);
	info = new Info(&alarm_state, gpio_pins, mac_adresses, device_place, esp_control);

	bme_sensor = new BME280("/dev/i2c-1");

	ualarm(100000, 100000);
	while (running)
		pause();

	/* Destructors */
	endwin();
	delete mac_adresses;
	delete menu;
	delete info;
	for (auto val : gpio_pins)
		delete val.second;
	delete bme_sensor;
	delete client;
	delete device_place;
	delete esp_control;
	delete place_control;
}
void stop_execution(int signum)
{
	running = false;
}
void sig_handler(int signum)
{
	cnt = (cnt + 1) % 20;
	if (running)
	{
		// Every 100 ms
		if (not mt_start)
		{
			mt_start = true;
			mt_finished = false;
			menu_thread = thread([]() {
				menu->update();
				mt_finished = true;
			});
		}

		// Every 1 second
		if (cnt % 10 == 0 and not it_start)
		{
			it_start = true;
			it_finished = false;
			info_thread = thread([]() {
				info->update();
				it_finished = true;
			});
		}

		// Every 1 second
		if (cnt % 10 == 0 and not sensor_start)
		{
			sensor_start = true;
			sensor_finish = false;
			sensor_thread = thread([]() {
				bme_sensor->updateData();
				for (auto val : gpio_pins)
					val.second->update();
				sensor_finish = true;
			});
		}

		if (motion_check() and alarm_state and not at_start)
		{
			at_start = true;
			at_finished = false;
			alarm_thread = thread([]() {
				system("omxplayer alarm.mp3 > /dev/null 2>&1");
				
				at_finished = true;
			});
		}

		/* Threads join */
		if (mt_start and mt_finished)
		{
			menu_thread.join();
			mt_start = false;
		}
		if (it_start and it_finished)
		{
			info_thread.join();
			it_start = false;
		}
		if (at_start and at_finished)
		{
			alarm_thread.join();
			at_start = false;
		}

		if (sensor_start and sensor_finish)
		{
			sensor_thread.join();
			sensor_start = false;
		}
	}
}

int on_message(void *context, char *topicName, int topic_size, MQTTClient_message *message)
{
	string topic_name, payload;

	/* Conversion from char * to string */
	int payload_size = strlen((char *)message->payload);
	int topic_name_size = strlen(topicName);
	for (int i = 0; i < topic_name_size; ++i)
		topic_name.push_back(topicName[i]);
	for (int i = 0; i < payload_size; ++i)
		payload.push_back(((char *)message->payload)[i]);

	message_handler(topic_name, json::parse(payload));

	/* Free of pointers */
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
void message_handler(string topic_name, json payload)
{
	
	string prefix;

	/* New device */
	prefix = MQTT_SUBSCRIBE_TOPIC.substr(0, 18);
	if (topic_name.substr(0, prefix.size()) == prefix)
	{

		string suffix = topic_name.substr(prefix.size());
		int j = (int)(find(suffix.begin(), suffix.end(), '/') - suffix.begin());
		string sub_topic = suffix.substr(0, j);

		/* New Device */
		if (sub_topic == "dispositivos")
		{
			string mac_address = suffix.substr(j + 1);
			if ((int)mac_adresses->size() < 5 and
				find(mac_adresses->begin(), mac_adresses->end(), mac_address) == mac_adresses->end())
				mac_adresses->push_back(mac_address);
		}
		else
		{
			/* Place info */
			string type = suffix.substr(j + 1);
			string mac_address = payload["mac_address"];

			if (type == "temperatura")
			{
				int temperature = payload["temperatura"];
				(*esp_control)[mac_address].temperatura = temperature;
			}
			else if (type == "umidade")
			{
				int humidity = payload["umidade"];
				(*esp_control)[mac_address].umidade = humidity;
			}
			else if (type == "estado")
			{
				int state = payload["estado"];
				(*esp_control)[mac_address].entrada = state;
			}
		}
	}
	
}

bool motion_check()
{
	bool motion = false;
	vector<int> devices{
		MOTION1, MOTION2,
		OPENING1, OPENING2, OPENING3, OPENING4};

	string device_name = "";
	for (int device : devices){
		if (gpio_pins[device]->getPinState()){

			motion = true;
		}
	}
	if(motion and alarm and not is_playing){
		registerLog(menu->getLogName(), "Acionamento do alarme", "-");
		is_playing = true;
	}
	if(not motion and is_playing){
		is_playing = false;
	}
	return motion;
}