#include "mqtt.hpp"
#include <string>
#include <iostream>
using namespace std;

ClientMQTT::ClientMQTT(string adress, string client_id,
                       int (*on_message)(void *context, char *topicName, int topicLen, MQTTClient_message *message),
                       int persistence_type, void *persistence_context,
                       MQTTClient_connectOptions connect_options)
{
    this->connect_options = connect_options;
    MQTTClient_create(&this->client, (char *)adress.c_str(), (char *)client_id.c_str(), persistence_type, persistence_context);
    MQTTClient_setCallbacks(client, NULL, NULL, on_message, NULL);
}

int ClientMQTT::connect()
{
    int res = MQTTClient_connect(this->client, &this->connect_options);
    if (res != MQTTCLIENT_SUCCESS)
    {
        cerr << "Falha na conexao ao broker MQTT. Erro: " << res << endl;
        return -1;
    }
    return 0;
}

void ClientMQTT::publish(string topic, string payload, int qos, int retained)
{
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    pubmsg.payload = (void *)payload.c_str();
    pubmsg.payloadlen = payload.size();
    pubmsg.qos = qos;
    pubmsg.retained = retained;
    MQTTClient_deliveryToken token;
    MQTTClient_publishMessage(client, (char *)topic.c_str(), &pubmsg, &token);
    MQTTClient_waitForCompletion(client, token, 1000L);
}

void ClientMQTT::subscribe(string topic, int qos)
{
    MQTTClient_subscribe(this->client, (char *)topic.c_str(), qos);
}
