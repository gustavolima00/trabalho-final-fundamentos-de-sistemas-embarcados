#ifndef GPIO_H
#define GPIO_H
#include <bcm2835.h>
#define INPUT BCM2835_GPIO_FSEL_INPT
#define OUTPUT BCM2835_GPIO_FSEL_OUTP
#define LAMP1 17
#define LAMP2 18
#define LAMP3 27
#define LAMP4 22
#define AIR1 23
#define AIR2 24
#define MOTION1 25
#define MOTION2 26
#define OPENING1 05
#define OPENING2 06
#define OPENING3 12
#define OPENING4 16
#define OPENING5 20
#define OPENING6 21

void initGPIO();
class GPIOPin{
    private:
        int pin_number;
        int pin_mode;
        void (*onTurnOn)(void);
        bool state;
    public:
        GPIOPin(int pin_number, int pin_mode, void (*onTurnOn)(void) = [](){});
        bool getPinState();
        void update();
        void setPinState(int state);
};
#endif