#ifndef MQTT_HPP
#define MQTT_HPP
#include <MQTTClient.h>
#include <string>

class ClientMQTT
{
private:
    MQTTClient client;
    MQTTClient_connectOptions connect_options;

public:
    ClientMQTT(
        std::string adress, std::string client_id,
        int (*on_message)(void *context, char *topicName, int topicLen, MQTTClient_message *message) =  [](void *context, char *topicName, int topicLen, MQTTClient_message *message){ return 0;},
        int persistence_type = 0, void *persistence_context = NULL,
        MQTTClient_connectOptions connect_options = MQTTClient_connectOptions_initializer);

    int connect();
    void publish(std::string topic, std::string payload, int qos = 0, int retained = 0);
    void subscribe(std::string topic, int qos=0);
};

#endif