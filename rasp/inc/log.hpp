#ifndef LOG_H
#define LOG_H
#include <string>

std::string initLog();

void registerLog(std::string log_name, std::string event, std::string device);

#endif
