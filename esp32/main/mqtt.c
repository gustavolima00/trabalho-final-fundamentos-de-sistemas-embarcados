#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_system.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "gpio.h"
#include "driver/gpio.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "cJSON.h"

#include "mqtt.h"

#define TAG "MQTT"

extern xSemaphoreHandle conexaoMQTTSemaphore;
extern char *matricula;
extern char mac_adress[20];
extern char mac_adress[20];
extern char temp_addr[512];
extern char hum_addr[512];
extern char state_addr[512];
esp_mqtt_client_handle_t client;

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    static char *inicial_topic;

    switch (event->event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");

        xSemaphoreGive(conexaoMQTTSemaphore);
        inicial_topic = NULL;

        /* Getting topic name */
        if (inicial_topic != NULL)
            free(inicial_topic);
        int size = strlen("fse2020//dispositivos/") + strlen(matricula) + strlen(mac_adress) + 1;
        inicial_topic = (char *)malloc(sizeof(char) * size);
        sprintf(inicial_topic, "fse2020/%s/dispositivos/%s", matricula, mac_adress);
        mqtt_send_message(inicial_topic, "{}");
        msg_id = esp_mqtt_client_subscribe(client, inicial_topic, 0);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
        if (inicial_topic != NULL)
            free(inicial_topic);
        inicial_topic = NULL;
        break;
    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG, "MQTT_EVENT_DATA");
        char *topic_name;
        char *data;
        topic_name = (char *)malloc(sizeof(char) * (event->topic_len + 1));
        data = (char *)malloc(sizeof(char) * (event->data_len + 1));

        sprintf(topic_name, "%.*s", event->topic_len, event->topic);
        sprintf(data, "%.*s", event->data_len, event->data);

        if (strcmp(topic_name, inicial_topic) == 0)
        {
            cJSON *json = cJSON_Parse(data);
            /* Getting state*/
            cJSON *obj = cJSON_GetObjectItemCaseSensitive(json, "saida");
            if (obj != NULL)
            {
                gpio_set_level(LED_PIN, obj->valueint);
            }

            /* Getting place*/
            cJSON *place = cJSON_GetObjectItemCaseSensitive(json, "comodo");
            if (place != NULL)
            {
                sprintf(temp_addr, "fse2020/%s/%s/temperatura", matricula, place->valuestring);
                sprintf(hum_addr, "fse2020/%s/%s/umidade", matricula, place->valuestring);
                sprintf(state_addr, "fse2020/%s/%s/estado", matricula, place->valuestring);
                msg_id = esp_mqtt_client_subscribe(client, state_addr, 0);
            }
        }
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
        break;
    default:
        ESP_LOGI(TAG, "Other event id:%d", event->event_id);
        break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

void mqtt_start()
{
    esp_mqtt_client_config_t mqtt_config = {
        .uri = "mqtt://mqtt.eclipseprojects.io",
    };
    client = esp_mqtt_client_init(&mqtt_config);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
}

void mqtt_send_message(char *topico, char *mensagem)
{
    int message_id = esp_mqtt_client_publish(client, topico, mensagem, 0, 1, 0);
    ESP_LOGI(TAG, "Mensagem enviada, ID: %d", message_id);
}
