/* Includes */
#include <stdio.h>
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "freertos/semphr.h"
#include "gpio.h"
#include "wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include <string.h>
#include "mqtt.h"
#include "dht11.h"

/* DEFINES */
#define LED_DELAY 1000        // 1s
#define REQUEST_DELAY 30000   // 3s
#define SENSOR_TRY_DELAY 1000 // 1s

/* Semaphores */
xSemaphoreHandle connectedWifiSemaphore;
xSemaphoreHandle ledHandlerSemaphore;
xSemaphoreHandle conexaoMQTTSemaphore;

/* Global variables*/
bool connected = false;
const char *matricula = "170035158";
char mac_adress[20];
char temp_addr[512];
char hum_addr[512];
char state_addr[512];
char *place;
int temperature;
int humidity;

/* Handler funcitons header */
void connectionHandler(void *params);
void mqttHandler(void *params);
static void IRAM_ATTR gpio_isr_handler(void *args);
void buttonHandler(void *params);
void load_mac();

xQueueHandle handlerQueue;

/* Main */
void app_main(void)
{
  hum_addr[0] = '\0';
  temp_addr[0] = '\0';
  state_addr[0] = '\0';

  /* Getting mac adress */
  load_mac();

  /* GPIO inits */
  button_init();
  led_init();
  gpio_set_level(LED_PIN, 0);
  DHT11_init(SENSOR_PIN);

  fprintf(stderr, "Mac Address: %s\n", mac_adress);
  /* Init of NVS */
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
  {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  /* Semaphores inits */
  handlerQueue = xQueueCreate(10, sizeof(int));
  connectedWifiSemaphore = xSemaphoreCreateBinary();
  conexaoMQTTSemaphore = xSemaphoreCreateBinary();

  /* Tasks configs */
  xTaskCreate(buttonHandler, "Trata o botão", 2048, NULL, 1, NULL);
  xTaskCreate(&connectionHandler, "Cuida das conexoes", 2048, NULL, 1, NULL);
  xTaskCreate(&mqttHandler, "Comunicação com Broker", 4096, NULL, 1, NULL);

  gpio_install_isr_service(0);
  gpio_isr_handler_add(BUTTON_PIN, gpio_isr_handler, (void *)BUTTON_PIN);

  wifi_start();
}

void mqttHandler(void *params)
{

  if (xSemaphoreTake(conexaoMQTTSemaphore, portMAX_DELAY))
  {
    while (true)
    {
      if (temp_addr[0])
      {
        struct dht11_reading data = DHT11_read();
        if (data.status != 0)
        {
          ESP_LOGE("DHT11 ERROR", "DHT11 FAIL TO READ SENSOR");
          vTaskDelay(SENSOR_TRY_DELAY / portTICK_PERIOD_MS);
          continue;
        }
        else
        {
          char message[512];

          /* Sending Temperature*/
          sprintf(message, "{\"temperatura\": %d, \"mac_address\":\"%s\"}", data.temperature, mac_adress);
          mqtt_send_message(temp_addr, message);

          /* Sending Pressure*/
          sprintf(message, "{\"umidade\": %d, \"mac_address\":\"%s\"}", data.humidity, mac_adress);
          mqtt_send_message(hum_addr, message);
        }
        vTaskDelay(REQUEST_DELAY / portTICK_PERIOD_MS);
      }
    }
  }
}

void connectionHandler(void *params)
{
  while (true)
  {
    if (xSemaphoreTake(connectedWifiSemaphore, portMAX_DELAY))
    {
      ESP_LOGI("Main Task", "WIFI Connected");
      mqtt_start();
    }
  }
}

static void IRAM_ATTR gpio_isr_handler(void *args)
{
  int pino = (int)args;
  xQueueSendFromISR(handlerQueue, &pino, NULL);
}

void buttonHandler(void *params)
{
  int pino;
  int state = 0;

  while (true)
  {
    if (xQueueReceive(handlerQueue, &pino, portMAX_DELAY))
    {

      // De-bouncing
      int estado = gpio_get_level(pino);
      if (estado == 1)
      {
        gpio_isr_handler_remove(pino);
        while (gpio_get_level(pino) == estado)
        {
          vTaskDelay(50 / portTICK_PERIOD_MS);
        }
        state = (state + 1) % 2;
        fprintf(stderr, "Button state: %d\n", state);

        if (state_addr[0])
        {
          char message[512];
          fprintf(stderr, "state_addr: %s\n", state_addr);
          /* Sending state*/
          sprintf(message, "{\"estado\": %d, \"mac_address\":\"%s\"}", state, mac_adress);
          mqtt_send_message(state_addr, message);
        }
        // Habilitar novamente a interrupção
        vTaskDelay(50 / portTICK_PERIOD_MS);
        gpio_isr_handler_add(pino, gpio_isr_handler, (void *)pino);
      }
    }
  }
}
void load_mac()
{
  uint8_t mac[6];
  esp_efuse_mac_get_default(mac);
  if (esp_efuse_mac_get_default(mac) != ESP_OK)
  {
    fprintf(stderr, "Erro na leitura do mac Adress\n");
  }
  else
  {
    sprintf(mac_adress, "%02x:%02x:%02x:%02x:%02x:%02x",
            (uint8_t)mac[0], (uint8_t)mac[1],
            (uint8_t)mac[2], (uint8_t)mac[3],
            (uint8_t)mac[4], (uint8_t)mac[5]);
  }
}