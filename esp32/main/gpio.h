#ifndef GPIO_H
#define GPIO_H

#define LED_PIN 2
#define BUTTON_PIN 0
#define SENSOR_PIN 4

void button_init();
void led_init();
void turn_off_led();
void turn_on_led();

#endif
